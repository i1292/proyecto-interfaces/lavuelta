<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\SqlDataProvider;
use yii\data\ActiveDataProvider;
use app\models\Ciclista;
use app\models\Equipo;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->actionTopciclistas();
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    public function actionDatos(){
        return $this->render('datos');
    }   
    
    public function actionToptres(){
        $dataProvider = new SqlDataProvider([
            'sql'=> "SELECT nombre as mejoresciclistas FROM(SELECT e.dorsal, COUNT(*) AS victorias, c.nombre FROM etapa e LEFT JOIN ciclista c ON e.dorsal = c.dorsal GROUP BY dorsal ORDER BY victorias DESC limit 3) AS a",
        ]);
        
        return $this->render("resultadolistview",[
            "dataProvider"=>$dataProvider,
        ]);
    }
    
    /*
     * esta funcion se ejecuta desde la funcion actionIndex, lo que hace es realizar una consulta y devuelve un render 
     * y una variable donde se guardan los datos de la consulta para poder usarlos en la vista
     */
    
    public function actionTopciclistas(){
        $topEquipos = $this->actionTopequipos();
        $topEscaladores = $this->actionTopescaladores();
        $dorsalGanador = Yii::$app->db->createCommand("SELECT nombre FROM(SELECT e.dorsal, COUNT(*) AS victorias, c.nombre FROM etapa e LEFT JOIN ciclista c ON e.dorsal = c.dorsal GROUP BY dorsal ORDER BY victorias DESC limit 3) AS a")->queryAll();
        
        return $this->render("_resultadolistview",[
            "dorsal"=>$dorsalGanador,
            "topequipos"=>$topEquipos,
            "topescaladores"=>$topEscaladores,
        ]);
    }
    
    /*
     * funcion que es llamada por actionTopciclistas, realiza una consulta y 
     * devuelve una variable con los datos de la consulta
     */
    
    public function actionTopequipos(){
        $topEquipos = Yii::$app->db->createCommand("select nomequipo from(SELECT e.dorsal, COUNT(*) AS victorias, c.nomequipo FROM etapa e LEFT JOIN ciclista c ON e.dorsal = c.dorsal GROUP BY c.nomequipo ORDER BY victorias DESC LIMIT 3) AS a")->queryAll();
        
        return $topEquipos;
    }
    
    /*
     * funcion que usamos para calcular los tres mejores escaladores de la vuelta y nos lo retorna en una 
     * variable
     */
    
    public function actionTopescaladores(){
        $topEscaladores = Yii::$app->db->createCommand("SELECT nombre FROM (SELECT p.dorsal, c.nombre, COUNT(*) AS victorias FROM  puerto p LEFT JOIN ciclista c ON p.dorsal = c.dorsal GROUP BY p.dorsal ORDER BY victorias DESC LIMIT 3) AS a")->queryAll();
        
        return $topEscaladores;
    }
}


/*
 * consulta top 3 ciclista de la vuelta
 * 
 * SELECT nombre FROM(SELECT e.dorsal, COUNT(*) AS victorias, c.nombre FROM etapa e LEFT JOIN ciclista c ON e.dorsal = c.dorsal GROUP BY dorsal ORDER BY victorias DESC limit 3) AS a
 */

/*
 * consulta top 3 mejores equipos
 * 
 * select nomequipo from(SELECT e.dorsal, COUNT(*) AS victorias, c.nomequipo FROM etapa e LEFT JOIN ciclista c ON e.dorsal = c.dorsal GROUP BY c.nomequipo ORDER BY victorias DESC LIMIT 3) AS a
 */

/*
 * consulta usada para sacar los tres mejores escaladores de la vielta
 * SELECT nombre FROM (SELECT p.dorsal, c.nombre, COUNT(*) AS victorias FROM  puerto p LEFT JOIN ciclista c ON p.dorsal = c.dorsal GROUP BY p.dorsal ORDER BY victorias DESC LIMIT 3) AS a
 */