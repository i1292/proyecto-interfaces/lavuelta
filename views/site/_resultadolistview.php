<?php
//use yii\widgets\ListView;
use yii\helpers\Html;
$this->title = 'La vuelta';
?>
<div class="pantalla">
    <div class="size-carousel">
        <!--Carousel Wrapper-->
        <div id="carousel-example-2" class="carousel slide carousel-fade" data-ride="carousel">
          <!--Indicators-->
          <ol class="carousel-indicators">
            <li data-target="#carousel-example-2" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-2" data-slide-to="1"></li>
            <li data-target="#carousel-example-2" data-slide-to="2"></li>
          </ol>
          <!--/.Indicators-->
          <!--Slides-->
          <div class="carousel-inner" role="listbox">
            <div class="carousel-item active">
              <div class="view">
                <?= Html::img("@web/images/1.png",['class'=>"d-block foto-carousel"])?>
                <div class="mask rgba-black-light"></div>
              </div>
                <div class="carousel-caption" style="padding-bottom: 420px">
                    <h3 class="h3-responsive">Top 3 ciclistas de la vuelta </h3>
                </div>
                <div class="carousel-caption" style="padding-bottom: 10px">
                <p>1º. <?= implode($dorsal[0])?></p>
                <p>2º. <?= implode($dorsal[1])?> </p>
                <p>3º. <?= implode($dorsal[2])?></p>
              </div>
            </div>
            <div class="carousel-item size-carousel">
              <!--Mask color-->
              <div class="view">
                <?= Html::img("@web/images/2.png",['class'=>"d-block foto-carousel"])?>
                <div class="mask rgba-black-strong"></div>
              </div>
                <div class="carousel-caption" style="padding-bottom: 420px">
                    <h3 class="h3-responsive">Top 3 mejores equipos </h3>
                </div>
                <div class="carousel-caption" style="padding-bottom: 10px">
                <p>1º. <?= implode($topequipos[0])?></p>
                <p>2º. <?= implode($topequipos[1])?> </p>
                <p>3º. <?= implode($topequipos[2])?></p>
              </div>
            </div>
            <div class="carousel-item size-carousel">
              <!--Mask color-->
              <div class="view">
                <?= Html::img("@web/images/3.png",['class'=>"d-block foto-carousel"])?>
                <div class="mask rgba-black-slight"></div>
              </div>
                <div class="carousel-caption" style="padding-bottom: 420px">
                    <h3 class="h3-responsive">Top 3 mejores escaladores de montaña </h3>
                </div>
                <div class="carousel-caption" style="padding-bottom: 10px">
                <p>1º. <?= implode($topescaladores[0])?></p>
                <p>2º. <?= implode($topescaladores[1])?> </p>
                <p>3º. <?= implode($topescaladores[2])?></p>
              </div>
            </div>
          </div>
          <!--/.Slides-->
          <!--Controls-->
          <a class="carousel-control-prev" href="#carousel-example-2" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carousel-example-2" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
          <!--/.Controls-->
        </div>
        <!--/.Carousel Wrapper-->
    </div>
</div>